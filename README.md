### What is it? ###

Uses vorpal: immersive/interactive command line app. Uses sockets (not websockets).

### How do I get set up? ###

* npm install
* node server.js
* node client.js

https://github.com/dthree/vorpal

### How do I use it ###

In the client 'send foo' or 'send bar'

You should then see the 'Ascii1' or 'Ascii2' response.

'send xxx' will return 'Not a valid Ascii!!!'

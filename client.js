import MazeSolver from './mazeSolver'

const vorpal = require('vorpal')();

const net = require('net');
const HOST = '127.0.0.1';
const PORT = 8081;
//const HOST = '10.40.72.109';
//const PORT = 6000;
const client = new net.Socket();
const mazeSolver = new MazeSolver(client);

client.connect(PORT, HOST, function() {
  console.log('CONNECTED TO: ' + HOST + ':' + PORT);
});

vorpal
  .command('send [data]', 'Send to server".')
  .action(function(args, callback) {
    client.write(`${args.data}\n`);
    callback();
  });

vorpal
  .delimiter('asciiCLI$')
  .show();

client.on('data', function(data) {
  console.log('Message from server: ' + data);
  mazeSolver.parseResponse(data.toString());
  //client.destroy();
});

client.on('close', function() {
  console.log('Connection closed');
});

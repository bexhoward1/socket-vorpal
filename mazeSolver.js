class MazeSolver {

  constructor(client) {
    this.client = client;
    this.mem = [];
    this.negativeMapper = {
      'N': 'S',
      'S': 'N',
      'E': 'W',
      'W': 'E'
    };
  }

  onlyOneMoveAvailable(arr) {
    //[N0, W3, E3, S3]
    const resArray = arr;
    //return [{key:N, value:0}, ...]
    const splitToObj =  resArray.map((item) => {
      return { key: item[0], value: parseInt(item[1]) };
    });

    const nonZeros = splitToObj.filter((obj) => {
      return (obj.value !== 0 && obj.key !== 'P');
    });

    if (nonZeros.length === 1) {
      return nonZeros[0].key;
    }
  }

  canSeePrize(arr) {
    const resArray = arr;
    const getPValue =  arr[arr.length - 1].slice(-1);
    if (getPValue !== '?') {
      return getPValue;
    }
  }

  move(direction) {
    if (this.mem[this.mem.length-1] !== this.negativeMapper[direction]) {
      this.mem.push(this.negativeMapper[direction]);
    }

    console.log(direction);
    this.client.write(`${direction}\n`);
  }

  canMoveInDirection(resObj, direction) {
    return resObj[direction] > 0 && this.mem[this.mem.length-1] !== direction;
  }

  haveNotBeenHere(direction) {
    return this.mem[this.mem.length-1] !== direction && this.mem[this.mem.length-2] !== direction;
  }

  parseResponse(response) {
    if (response === "Language/team name?\n") return false;

    const resArr = response.split(" ");
    const resObj = resArr.reduce((acc, item) => {
      acc[item[0]] = parseInt(item[1]);
      return acc;
    }, {});

    if(resArr.includes("PX")) {
      console.log('Im there');
      return "I\'m there!";
    }

    const direction = this.canSeePrize(resArr) || this.onlyOneMoveAvailable(resArr);

    if (direction) {
      return this.move(direction);
    }

    if (this.canMoveInDirection(resObj, 'N')) {
      return this.move('N');
    }
    if (this.canMoveInDirection(resObj, 'E')) {
      return this.move('E');
    }
    if (this.canMoveInDirection(resObj, 'S') && this.haveNotBeenHere('S')) {
      return this.move('S');
    }
    if (this.canMoveInDirection(resObj, 'W')) {
      return this.move('W');
    }
  }

}

export default MazeSolver;

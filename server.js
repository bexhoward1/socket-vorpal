const net = require('net');

const HOST = '127.0.0.1';
const PORT = 8081;

net.createServer(function(sock) {
  console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);

  sock.on('data', function(ascii) {
    const code = ascii.toString('utf8');
    sock.write('N0 E2 S0 W0 PX');
    // switch (code) {
    //   case 'foo':
    //     sock.write('Ascii1');
    //     break;
    //   case 'bar':
    //     sock.write('Ascii2');
    //     break;
    //   default:
    //     sock.write('Not a valid Ascii!!!');
    // }
  });

  sock.on('close', function(data) {
    console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
  });

}).listen(PORT, HOST);

console.log('Server listening on ' + HOST +':'+ PORT);

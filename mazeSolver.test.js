import MazeSolver from './mazeSolver';

let mazeSolver;

const client = {
  write: jest.fn()
};

beforeEach(() => {
  mazeSolver = new MazeSolver(client);
});

test('Should return if teamname string is received', () => {
  expect(mazeSolver.parseResponse('Language/team name?\n')).toBe(false);
});

test('Should go direction available when a dead end', () => {
  mazeSolver.parseResponse('N0 E0 S0 W4 P?');
  expect(client.write).toBeCalledWith('W\n');
});

test('Should go N if PN', () => {
  mazeSolver.parseResponse('N0 E5 S5 W4 PN');
  expect(client.write).toBeCalledWith('N\n');
});

test('Should solver path 1', () => {
  mazeSolver.parseResponse('N0 E0 S0 W1 P?');
  expect(client.write).lastCalledWith('W\n');

  mazeSolver.parseResponse('N3 E1 S2 W0 P?');
  expect(client.write).lastCalledWith('N\n');

  mazeSolver.parseResponse('N2 E0 S3 W0 P?');
  expect(client.write).lastCalledWith('N\n');

  mazeSolver.parseResponse('N1 E0 S4 W0 P?');
  expect(client.write).lastCalledWith('N\n');

  mazeSolver.parseResponse('N0 E5 S5 W0 P?');
  expect(client.write).lastCalledWith('E\n');

  mazeSolver.parseResponse('N0 E4 S0 W1 P?');
  expect(client.write).lastCalledWith('E\n');

  mazeSolver.parseResponse('N0 E3 S2 W2 P?');
  expect(client.write).lastCalledWith('E\n');

  mazeSolver.parseResponse('N0 E2 S0 W3 P?');
  expect(client.write).lastCalledWith('E\n');

  mazeSolver.parseResponse('N0 E1 S2 W4 P?');
  expect(client.write).lastCalledWith('E\n');

  mazeSolver.parseResponse('N0 E0 S0 W5 P?');
  expect(client.write).lastCalledWith('W\n');

  mazeSolver.parseResponse('N0 E1 S2 W4 P?');
  expect(client.write).lastCalledWith('S\n');

  mazeSolver.parseResponse('N1 E0 S1 W0 P?');
  expect(client.write).lastCalledWith('S\n');

  mazeSolver.parseResponse('N2 E1 S0 W0 P?');
  expect(client.write).lastCalledWith('E\n');

  mazeSolver.parseResponse('N0 E0 S3 W1 P?');
  expect(client.write).lastCalledWith('S\n');

  mazeSolver.parseResponse('N1 E0 S2 W0 P?');
  expect(client.write).lastCalledWith('S\n');

  mazeSolver.parseResponse('N2 E0 S1 W0 P?');
  expect(client.write).lastCalledWith('S\n');

  mazeSolver.parseResponse('N3 E0 S0 W2 P?');
  expect(client.write).lastCalledWith('W\n');

  mazeSolver.parseResponse('N0 E1 S0 W1 P?');
  expect(client.write).lastCalledWith('W\n');

  mazeSolver.parseResponse('N2 E2 S0 W0 PN');
  expect(client.write).lastCalledWith('N\n');

  mazeSolver.parseResponse('N1 E0 S1 W0 PN');
  expect(client.write).lastCalledWith('N\n');

  mazeSolver.parseResponse('N0 E0 S2 W0 PX');
  expect(mazeSolver.parseResponse('N0 E0 S2 W0 PX')).toBe("I\'m there!");
});

// test('Should solver path 2', () => {
//   mazeSolver.parseResponse('N0 E0 S5 W0 P?');
//   expect(client.write).lastCalledWith('S\n');

//   mazeSolver.parseResponse('N1 E0 S4 W0 P?');
//   expect(client.write).lastCalledWith('S\n');

//   mazeSolver.parseResponse('N2 E0 S3 W0 P?');
//   expect(client.write).lastCalledWith('S\n');

//   mazeSolver.parseResponse('N3 E0 S2 W0 P?');
//   expect(client.write).lastCalledWith('S\n');

//   mazeSolver.parseResponse('N0 E4 S1 W0 P?');
//   expect(client.write).lastCalledWith('E\n');

//   mazeSolver.parseResponse('N0 E3 S0 W1 P?');
//   expect(client.write).lastCalledWith('E\n');


//   mazeSolver.parseResponse('N1 E2 S1 W2 P?');
//   expect(client.write).lastCalledWith('N\n');

//   mazeSolver.parseResponse('N0 E0 S2 W0 P?');
//   expect(client.write).lastCalledWith('S\n');

//   mazeSolver.parseResponse('N1 E2 S1 W2 P?');
//   expect(client.write).lastCalledWith('E\n');

//   mazeSolver.parseResponse('N0 E1 S0 W3 P?');
//   expect(client.write).lastCalledWith('E\n');

//   mazeSolver.parseResponse('N2 E0 S1 W4 P?');
//   expect(client.write).lastCalledWith('N\n');

//   mazeSolver.parseResponse('N1 E0 S2 W0 P?');
//   expect(client.write).lastCalledWith('N\n');

//   mazeSolver.parseResponse('N0 E0 S3 W1 PW');
//   expect(client.write).lastCalledWith('W\n');

//   mazeSolver.parseResponse('N0 E1 S0 W0 PX');
//   expect(mazeSolver.parseResponse('N0 E1 S0 W0 PX')).toBe("I\'m there!");
// });
